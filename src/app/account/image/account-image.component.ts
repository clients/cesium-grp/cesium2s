import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, Renderer2 } from '@angular/core';
import { Account } from '@app/account/account.model';

@Component({
  selector: 'app-account-image',
  templateUrl: './account-image.component.html',
  styleUrls: ['./account-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountImageComponent {
  @Input() account: Account;

  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    private _cd: ChangeDetectorRef
  ) {}

  onImageLoaded(avatar: string) {
    console.debug('[app-account-image] onImageLoaded ', avatar);
    this.renderer.addClass(this.el.nativeElement, 'loaded');
    this._cd.markForCheck();
  }
}
