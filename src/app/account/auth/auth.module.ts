import { NgModule } from '@angular/core';
import { AuthForm } from './auth.form';
import { AuthModal } from './auth.modal';
import { AppSharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MnemonicForm } from './mnemonic/mnemonic.form';
import { AppRegisterModule } from '@app/account/register/register.module';
import { PubkeyForm } from '@app/account/auth/pubkey/pubkey.form';
import { AddressForm } from '@app/account/auth/address/address.form';
import { DerivationSelectionComponent } from '@app/account/auth/derivation-selection/derivation-selection.component';

@NgModule({
  imports: [
    // App modules
    AppSharedModule,
    AppRegisterModule,
  ],
  declarations: [AuthForm, AuthModal, MnemonicForm, PubkeyForm, AddressForm, DerivationSelectionComponent],
  exports: [AuthForm, AuthModal, MnemonicForm, PubkeyForm, AddressForm, DerivationSelectionComponent, TranslateModule],
})
export class AppAuthModule {}
