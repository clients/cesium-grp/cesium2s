import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AccountsService } from '@app/account/accounts.service';
import { ModalController } from '@ionic/angular';
import { DerivationAccount } from '@app/account/account.model';
import { isNotEmptyArray } from '@app/shared/functions';

@Component({
  selector: 'app-derivation-selection',
  templateUrl: 'derivation-selection.component.html',
  styleUrls: ['./derivation-selection.component.scss'],
})
export class DerivationSelectionComponent implements OnInit {
  @Input() mnemonic: string;
  @Input() derivations: DerivationAccount[];

  selectedDerivation: DerivationAccount;
  loading: boolean = true;

  get invalid(): boolean {
    return this.selectedDerivation == null;
  }

  constructor(
    private accountService: AccountsService,
    private cd: ChangeDetectorRef,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    if (isNotEmptyArray(this.derivations)) {
      this.loading = false;
    } else if (this.mnemonic) {
      this.scanDerivations();
    }
  }

  async scanDerivations() {
    this.loading = true;
    this.cd.detectChanges();

    try {
      this.derivations = await this.accountService.scanDerivations(this.mnemonic);
    } finally {
      this.loading = false;
      this.cd.detectChanges();
    }
  }

  selectDerivation(derivation: DerivationAccount) {
    this.selectedDerivation = derivation;
    this.cd.detectChanges();
  }

  onSelectionChange() {
    this.cd.detectChanges();
  }

  async doSubmit() {
    if (this.selectedDerivation) {
      await this.modalCtrl.dismiss(this.selectedDerivation);
    }
  }

  async onCancel() {
    await this.modalCtrl.dismiss();
  }
}
