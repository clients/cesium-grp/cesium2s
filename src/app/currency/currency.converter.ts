import { LightUniversalDividendFragment, UniversalDividendConnection } from '../network/indexer/indexer-types.generated';
import { CurrencyHistory } from '@app/currency/currency-history.model';

export class CurrencyConverter {
  static squidConnectionToCurrency(universalDividendConnection: UniversalDividendConnection, debug?: boolean): CurrencyHistory[] {
    const inputs = universalDividendConnection.edges?.map((edge) => {
      return edge.node;
    }) as CurrencyHistory[];
    const results = (inputs || []).map(this.squidToCurrency);
    if (debug) console.debug('Results -> ', results);
    return results;
  }

  static squidToCurrency(input: LightUniversalDividendFragment): CurrencyHistory {
    if (!input) return undefined;
    return <CurrencyHistory>{
      membersCount: input.membersCount,
      blockNumber: input.blockNumber,
      timestamp: input.timestamp,
      monetaryMass: input.monetaryMass,
    };
  }
}
