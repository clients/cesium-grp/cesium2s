import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NetworkService } from '@app/network/network.service';
import { AppPage, AppPageState } from '@app/shared/pages/base-page.class';
import { TranslateService } from '@ngx-translate/core';
import { RxState } from '@rx-angular/state';
import { RxStateProperty, RxStateSelect } from '@app/shared/decorator/state.decorator';
import { SettingsService } from '@app/settings/settings.service';
import { CurrencyDisplayUnit } from '@app/settings/settings.model';
import { map, switchMap } from 'rxjs/operators';
import { isNotEmptyArray, toBoolean, toNumber } from '@app/shared/functions';
import { CurrencyHistory } from '@app/currency/currency-history.model';
import { ChartDataset, ChartOptions } from 'chart.js';
import { Observable } from 'rxjs';
import { IndexerService } from '@app/network/indexer/indexer.service';
import { Currency } from '@app/currency/currency.model';
import moment from 'moment';
import { Color } from '@app/shared/colors/graph-colors';

export interface CurrencyParameters {
  currencyName: string;
  currencyNetwork: string;
  currencySymbol: SafeHtml;
  members: number;
  monetaryMass: number;
  currentUd: number;
  ud0: number;
  udCreationPeriodMs: number;
  udReevalPeriodMs: number;
  growthRate: number;
  pastReevals: number;
}

export interface WotParameters {
  sigQtyRule: number;
  sigWindow: number;
  sigValidity: number;
  sigStock: number;
  sigPeriod: number;
  msWindow: number;
  msValidity: number;
  stepMax: number;
  sentries: number;
  xPercent: number;
}

export interface CurrencyPageState extends AppPageState {
  currency: Currency;
  params: CurrencyParameters;
  wotParams: WotParameters;
  paramsByUnit: Map<CurrencyDisplayUnit, CurrencyParameters>;
  useRelativeUnit: boolean;
  displayUnit: CurrencyDisplayUnit;
  showAllRulesCurrency: boolean;
  showAllRulesWot: boolean;
  currencyHistory: CurrencyHistory[];
  chartMembersData: ChartDataset<'line'>[];
  chartLabels: string[];
  chartLoaded: boolean;
  chartMonetaryMassData: ChartDataset<'line'>[];
  isLogarithmic: boolean;
  chartReady: boolean;
  membersVariation: number;
}

@Component({
  selector: 'app-currency',
  templateUrl: './currency.page.html',
  styleUrls: ['./currency.page.scss'],
  providers: [RxState],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurrencyPage extends AppPage<CurrencyPageState> {
  @RxStateSelect() protected chartMembersData$: Observable<ChartDataset<'line'>[]>;
  @RxStateSelect() protected chartMonetaryMassData$: Observable<ChartDataset<'line'>[]>;
  @RxStateSelect() protected chartLabels$: Observable<string[]>;
  @RxStateSelect() protected chartReady$: Observable<string[]>;
  @RxStateSelect() protected membersVariation$: Observable<number>;

  @RxStateProperty() protected params: CurrencyParameters;
  @RxStateProperty() protected wotParams: WotParameters;
  @RxStateProperty() protected useRelativeUnit: boolean;
  @RxStateProperty() protected currencyHistory: CurrencyHistory[];
  @RxStateProperty() protected chartMonetaryMassData: CurrencyHistory[];
  @RxStateProperty() protected chartMembersData: ChartDataset<'line'>[];
  @RxStateProperty() protected chartLabels: string[];
  @RxStateProperty() protected chartLoaded: boolean;
  @RxStateProperty() protected chartReady: boolean;
  @RxStateProperty() protected membersVariation: number;

  @Input() @RxStateProperty() showAllRulesCurrency: boolean;
  @Input() @RxStateProperty() showAllRulesWot: boolean;
  @Input() @RxStateProperty() displayUnit: CurrencyDisplayUnit;
  @Input() @RxStateProperty() isLogarithmic: boolean = false;

  public chartMembersOptions = <ChartOptions<'line'>>{
    responsive: true,
    maintainAspectRatio: true,
    interaction: {
      intersect: false,
      mode: 'index',
    },
    plugins: {
      title: {
        text: this.translate.instant('GRAPH.CURRENCY.MEMBERS_COUNT_TITLE'),
        display: true,
      },
    },
    scales: {
      x: {
        position: 'bottom',
      },
      y: {
        id: 'y-axis-1',
        ticks: {
          beginAtZero: false,
        },
      },
    },
    color: Color.get('primary').rgba(1),
  };
  public chartMonetaryMassOptions = <ChartOptions<'line'>>{
    responsive: true,
    plugins: {
      title: {
        text: this.translate.instant('GRAPH.CURRENCY.MONETARY_MASS_TITLE'),
        display: true,
      },
    },
    interaction: {
      intersect: false,
      mode: 'index',
    },
    scales: {
      x: {
        position: 'bottom',
      },
      y: {
        id: 'y-axis-1',
        ticks: {
          beginAtZero: false,
        },
      },
    },
    borderColor: Color.get('primary').rgba(1),
  };

  protected powBase: number;
  protected ud: number;

  constructor(
    protected networkService: NetworkService,
    protected translate: TranslateService,
    protected indexer: IndexerService,
    protected settings: SettingsService,
    protected cd: ChangeDetectorRef,
    private sanitizer: DomSanitizer
  ) {
    super({ name: 'currency-service' });

    // Watch settings
    this._state.connect('useRelativeUnit', this.settings.displayUnit$.pipe(map((unit) => unit === 'du')));

    this._state.connect('displayUnit', this._state.select('useRelativeUnit').pipe(map((useRelativeUnit) => (useRelativeUnit ? 'du' : 'base'))));

    this._state.connect(
      'params',
      this._state.select(['paramsByUnit', 'displayUnit'], (res) => res).pipe(map(({ paramsByUnit, displayUnit }) => paramsByUnit.get(displayUnit)))
    );

    this._state.connect('currencyHistory', this._state.select('currency').pipe(switchMap(() => this.indexer.loadCurrencyHistory())));
    this._state.connect('chartMembersData', this._state.select('currencyHistory').pipe(map((value) => this.prepareCurrencyMemberHistory(value))));
    this._state.connect(
      'chartMonetaryMassData',
      this._state
        .select(['currencyHistory', 'displayUnit', 'isLogarithmic'], (res) => res)
        .pipe(map(({ currencyHistory, displayUnit, isLogarithmic }) => this.prepareMonetaryMassHistory(currencyHistory, displayUnit, isLogarithmic)))
    );
    this._state.connect('membersVariation', this._state.select('currencyHistory').pipe(map((value) => this.getMembersVariation(value))));
    this._state.connect('chartLabels', this._state.select('currencyHistory').pipe(map((value) => this.prepareLineChartLabel(value))));
    this._state.connect(
      'chartReady',
      this._state
        .select(['chartLabels', 'chartMembersData', 'chartMonetaryMassData'], (res) => res)
        .pipe(
          map(
            ({ chartLabels, chartMembersData, chartMonetaryMassData }) =>
              isNotEmptyArray(chartLabels) && isNotEmptyArray(chartMembersData) && isNotEmptyArray(chartMonetaryMassData)
          )
        )
    );
  }

  protected async ngOnLoad(): Promise<Partial<CurrencyPageState>> {
    const showAllRulesCurrency = toBoolean(this.showAllRulesCurrency, false);
    const showAllRulesWot = toBoolean(this.showAllRulesWot, false);

    const useRelativeUnit = toBoolean(this.useRelativeUnit, false);
    const network = await this.networkService.ready();
    const api = network.api;
    const currency = network.currency;
    this.powBase = Math.pow(10, currency.decimals);
    const msPerBlock = toNumber(api.consts.babe.expectedBlockTime);
    const ud0 = toNumber(api.consts.universalDividend.unitsPerUd) / this.powBase;
    const [monetaryMassFractions, currentUd, members, pastReevals] = await Promise.all([
      api.query.universalDividend.monetaryMass(),
      api.query.universalDividend.currentUd(),
      api.query.membership.counterForMembership(),
      api.query.universalDividend.pastReevals(),
    ]);
    this.ud = toNumber(currentUd) / this.powBase;
    const growthRate = Math.sqrt(toNumber(api.consts.universalDividend.squareMoneyGrowthRate));

    const sigQtyRule = toNumber(api.consts.wot.minCertForMembership);
    const msWindow = toNumber(api.consts.distance.evaluationPeriod) * msPerBlock * 3;
    const msValidity = toNumber(api.consts.membership.membershipPeriod) * msPerBlock;
    const sigWindow = toNumber(api.consts.membership.membershipRenewalPeriod) * msPerBlock;
    const sigValidity = toNumber(api.consts.certification.validityPeriod) * msPerBlock;
    const sigStock = toNumber(api.consts.certification.maxByIssuer);
    const sigPeriod = toNumber(api.consts.certification.certPeriod) * msPerBlock;
    const stepMax = toNumber(api.consts.distance.maxRefereeDistance);
    const sentries = toNumber(api.consts.smithMembers.minCertForMembership);
    const xPercent = toNumber(api.consts.distance.minAccessibleReferees) / 1000000000;

    const paramsByUnit = new Map<CurrencyDisplayUnit, CurrencyParameters>();
    paramsByUnit.set('base', {
      currencyName: currency.displayName,
      currencySymbol: currency.symbol,
      currencyNetwork: currency.network,
      monetaryMass: toNumber(monetaryMassFractions) / this.powBase,
      members: toNumber(members),
      currentUd: this.ud,
      ud0,
      udCreationPeriodMs: toNumber(api.consts.universalDividend.udCreationPeriod),
      udReevalPeriodMs: toNumber(api.consts.universalDividend.udReevalPeriod),
      growthRate,
      pastReevals: toNumber(pastReevals[0][1]),
    });
    paramsByUnit.set('du', {
      currencyName: currency.displayName,
      currencySymbol: this.sanitizer.bypassSecurityTrustHtml(`${this.translate.instant('COMMON.UD')}<sub>${currency.symbol}</sub>`),
      currencyNetwork: currency.network,
      monetaryMass: toNumber(monetaryMassFractions) / this.powBase / this.ud,
      members: toNumber(members),
      currentUd: 1,
      ud0,
      udCreationPeriodMs: toNumber(api.consts.universalDividend.udCreationPeriod),
      udReevalPeriodMs: toNumber(api.consts.universalDividend.udReevalPeriod),
      growthRate,
      pastReevals: toNumber(pastReevals[0][1]),
    });

    const wotParams = <WotParameters>{
      sigQtyRule,
      sigWindow,
      sigValidity,
      sigStock,
      sigPeriod,
      msWindow,
      msValidity,
      stepMax,
      sentries,
      xPercent,
    };
    return { currency, paramsByUnit, showAllRulesCurrency, showAllRulesWot, useRelativeUnit, wotParams };
  }

  private prepareCurrencyMemberHistory(histories: CurrencyHistory[]): ChartDataset<'line'>[] {
    const dataset: ChartDataset<'line'> = {
      data: histories.map((data) => {
        return data.membersCount;
      }),
      label: this.translate.instant('GRAPH.CURRENCY.MEMBERS_COUNT_LABEL'),
      fill: true,
      tension: 0,
      borderWidth: 2,
      pointRadius: 0,
      pointHitRadius: 4,
      pointHoverRadius: 3,
      backgroundColor: Color.get('secondary').rgba(0.4),
    };
    this.chartMonetaryMassOptions.plugins = {
      title: {
        text: this.translate.instant('GRAPH.CURRENCY.MONETARY_MASS_TITLE'),
        display: true,
      },
    };

    if (this._debug) console.debug(`${this._logPrefix} number of member history generated: ${dataset.data.length}`);
    return <ChartDataset<'line'>[]>[dataset];
  }

  private prepareMonetaryMassHistory(histories: CurrencyHistory[], displayUnit: CurrencyDisplayUnit, isLogarithmic: boolean): ChartDataset<'line'>[] {
    const dataset: ChartDataset<'line'>[] = [
      {
        data: histories.map((data) => {
          if (displayUnit == 'base') {
            return data.monetaryMass / this.powBase;
          } else {
            return data.monetaryMass / this.powBase / this.ud;
          }
        }),

        label: this.translate.instant('GRAPH.CURRENCY.MONETARY_MASS_LABEL'),
        fill: true,
        tension: 0,
        borderWidth: 2,
        pointRadius: 0,
        pointHitRadius: 4,
        pointHoverRadius: 3,
        backgroundColor: Color.get('secondary').rgba(0.4),
        borderColor: Color.get('secondary').rgba(1),
      },
      {
        data: histories.map((data) => {
          if (displayUnit == 'base') {
            return data.monetaryMass / this.powBase / data.membersCount;
          } else {
            return data.monetaryMass / this.powBase / data.membersCount / this.ud;
          }
        }),
        label: this.translate.instant('GRAPH.CURRENCY.MONETARY_MASS_SHARE_LABEL'),
        borderColor: Color.parseRgba('rgb(255,255,0)').rgba(1),
        tension: 0,
        borderWidth: 2,
        pointRadius: 0,
        pointHitRadius: 4,
        pointHoverRadius: 3,
      },
    ];

    if (isLogarithmic) {
      this.chartMonetaryMassOptions = <ChartOptions<'line'>>{
        ...this.chartMonetaryMassOptions,
        scales: {
          y: {
            id: 'y-axis-1',
            type: 'logarithmic',
            ticks: {
              beginAtZero: false,
            },
          },
        },
      };
    } else {
      this.chartMonetaryMassOptions = <ChartOptions<'line'>>{
        ...this.chartMonetaryMassOptions,
        scales: {
          y: {
            id: 'y-axis-1',
            type: 'linear',
            ticks: {
              beginAtZero: false,
            },
          },
        },
      };
    }
    return dataset;
  }

  private prepareLineChartLabel(histories: CurrencyHistory[]): string[] {
    const labels = histories.map((data) => {
      return moment(data.timestamp).format('MM/YYYY');
    });

    if (this._debug) console.debug(`${this._logPrefix} number of label generated: ${labels.length}`);

    this.cd.markForCheck();
    return labels;
  }

  private getMembersVariation(histories: CurrencyHistory[]): number {
    const membersCountCurrent = histories[histories.length - 1];
    const membersCountPast = histories[histories.length - 2];
    return membersCountCurrent.membersCount - membersCountPast.membersCount;
  }
}
