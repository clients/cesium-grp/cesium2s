import { Moment } from 'moment/moment';

export class CurrencyHistory {
  constructor(membersCount?: number, blockNumber?: number, timestamp?: Date, monetaryMass?: number) {
    this.membersCount = membersCount;
    this.blockNumber = blockNumber;
    this.timestamp = timestamp;
    this.monetaryMass = monetaryMass;
  }

  membersCount: number;
  blockNumber: number;
  timestamp: Date;
  monetaryMass: number;
}

export interface CurrencyHistoryFilter {
  before?: Moment;
  after?: Moment;
}

export class ChartMembers {
  constructor(dataPoints: DataChart[], label: string) {
    this.dataPoints = dataPoints;
    this.label = label;
  }
  type: string;
  dataPoints: DataChart[];
  label: string;
}

export class DataChart {
  constructor(x: string, y: string) {
    this.x = x;
    this.y = y;
  }

  x: string;
  y: string;
}
