import { Injectable } from '@angular/core';
import { SettingsService } from '@app/settings/settings.service';
import { StartableService } from '@app/shared/services/startable-service.class';
import { Promise } from '@rx-angular/cdk/zone-less/browser';
import { IndexerService } from '@app/network/indexer/indexer.service';

@Injectable({ providedIn: 'root' })
export class CurrencyService extends StartableService {
  constructor(
    private indexer: IndexerService,
    private settings: SettingsService
  ) {
    super(settings);
  }

  protected ngOnStart(): Promise<void> {
    return Promise.resolve(undefined);
  }
}
