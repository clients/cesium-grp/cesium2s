import { NgModule } from '@angular/core';

import { AppSharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CurrencyPage } from './currency.page';
import { BaseChartDirective, provideCharts, withDefaultRegisterables } from 'ng2-charts';

@NgModule({
  imports: [AppSharedModule, TranslateModule.forChild(), BaseChartDirective],
  declarations: [CurrencyPage],
  exports: [CurrencyPage],
  providers: [provideCharts(withDefaultRegisterables())],
  bootstrap: [CurrencyPage],
})
export class AppCurrencyModule {
  constructor() {
    console.debug('[settings] Creating module');
  }
}
