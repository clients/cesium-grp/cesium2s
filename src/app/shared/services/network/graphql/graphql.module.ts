import { NgModule } from '@angular/core';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { ApolloModule } from 'apollo-angular';

@NgModule({ exports: [ApolloModule], imports: [ApolloModule],
  providers: [provideHttpClient(withInterceptorsFromDi())] })
export class AppGraphQLModule {
  constructor() {}
}
